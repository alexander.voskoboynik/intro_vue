import {api} from "@/axios-api";
import {ref, onMounted} from "vue";

export function usePosts(pageLimit) {
  const posts = ref([]);
  const totalPages =  ref(0);
  const isPostsLoading = ref(true);
  const fetching = async () => {
    try {
      const response = await api.get('posts', {
        params: {
          _page: 1,
          _limit: pageLimit
        }
      });
      totalPages.value = Math.ceil(response.headers['x-total-count'] / pageLimit)
      posts.value = response.data;
    } catch (e) {
      alert('!Error')
    } finally {
      isPostsLoading.value = false;
    }
  };
  onMounted(fetching);
  return {
    posts, isPostsLoading, totalPages
  }
}
