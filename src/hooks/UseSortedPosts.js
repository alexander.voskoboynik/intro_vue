import { ref, computed } from "vue";

export default  function useSortedPosts(posts) {
  const selectedSort = ref("");
  const sortedPosts = computed(() => {
    return  [...posts.value].sort((post, nextPost) => post[selectedSort.value]?.localeCompare(nextPost[selectedSort.value]));
  });

  return { sortedPosts, selectedSort };
};
