import {createRouter, createWebHistory} from "vue-router";
import Main from "@/pages/Main"
import PostsPage from "@/pages/PostsPage";
import PostDetail from "@/pages/PostDetail";
import VuexPosts from "@/pages/VuexPosts";
import CompositionPostPage from "@/pages/CompositionPostPage";

const routes = [
  { path: "/", name: "main", component: Main },
  { path: "/posts", name: "posts", component: PostsPage },
  { path: "/posts-x", name: "posts-x", component: VuexPosts },
  { path: "/posts/:id", name: "post-detail", component: PostDetail },
  { path: "/posts-c", name: "posts-c", component: CompositionPostPage }

]

const router = createRouter({
    routes,
    history: createWebHistory()
});

export default router;
