import { api } from "@/axios-api";

export const PostModule = {
  state: () => ({
    page: 1,
    pageLimit: 10,
    totalPages: 0,
    posts: [],
    dialogVisible: false,
    isPostsLoading: false,
    selectedSort: "",
    searchQuery: "",
    sortOptions: [
      {value: "title", name: "By name"},
      {value: "body", name: "By body"}
    ]
  }),
  getters: {
    sortedPosts(state) {
      return [...state.posts].sort((post, nextPost) => post[state.selectedSort]?.localeCompare(nextPost[state.selectedSort]))
    },
    sortedAndSearchedPosts(state, getters) {
      return getters.sortedPosts.filter((post) => post.title.toLowerCase().includes(state.searchQuery.toLowerCase()))
    }
  },
  mutations: {
    setPosts(state, payload) {
      state.posts = payload;
    },
    setLoading(state, bool) {
      state.isPostsLoading = bool
    },
    setPage(state, page) {
      state.page = page
    },
    setSelectedSort(state, selectedSort) {
      state.selectedSort = selectedSort
    },
    setTotalPages(state, totalPages) {
      state.totalPages = totalPages
    },
    setSearchQuery(state, searchQuery) {
      state.searchQuery = searchQuery
    },
  },
  actions: {
    async fetchPosts({ commit, state }) {
      try {
        commit("setLoading", true);
        const response = await api.get('posts', {
          params: {
            _page: state.page,
            _limit: state.pageLimit
          }
        });
        commit("setTotalPages", Math.ceil(response.headers['x-total-count'] / state.pageLimit));
        commit("setPosts", response.data);
      } catch (e) {
        alert('!Error');
      } finally {
        commit("setLoading", false);
      }
    },
    async loadMorePosts({ commit, state }) {
      try {
        commit("setPage", state.page + 1);
        const response = await api.get('posts', {
          params: {
            _page: state.page,
            _limit: state.pageLimit
          }
        });
        commit("setTotalPages", Math.ceil(response.headers['x-total-count'] / state.pageLimit));
        commit("setPosts", [...state.posts, ...response.data]);
      } catch (e) {
        alert('!Error')
      }
    }
  },
  namespaced: true
}
