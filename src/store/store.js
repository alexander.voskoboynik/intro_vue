import { createStore } from "vuex";
import { PostModule } from "@/store/PostModule";

const store = createStore ({
  state: {},
  getters: {},
  mutations: {},
  actions: {},
  modules: {
    posts: PostModule
  }
});

export default store;
